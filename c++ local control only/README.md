# blinds



This is a simple project that controls window blinds. It's intended to run on Raspberry Pi Pico.

Currently the project only controls one set of blinds using buttons, but eventualle the project should be able to control many sets of blinds using Google Home or some other voice-commanded AI assistant.