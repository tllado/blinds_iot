////////////////////////////////////////////////////////////////////////////////
// Living Room Blinds Control Code, now without Alexa!
// written by Travis Llado
// last edited 2025-02-20
////////////////////////////////////////////////////////////////////////////////

#define STARTUP_MON   2
#define STARTUP_DAY   20
#define STARTUP_HOUR  10
#define STARTUP_MIN   44

#define BLINDS_ARE_OPEN false

#define SUNRISE_MARGIN (30 * 60)  // sec
#define SUNSET_MARGIN  (30 * 60)  // sec

#define DEBUG_LED     1
#define DEBUG_SERIAL  0

////////////////////////////////////////////////////////////////////////////////

#define SUMMER_SOLSTICE_DATE          171
#define SUMMER_SOLSTICE_SUNRISE_HOUR  4
#define SUMMER_SOLSTICE_SUNRISE_MIN   47
#define SUMMER_SOLSTICE_SUNSET_HOUR   19
#define SUMMER_SOLSTICE_SUNSET_MIN    34

#define WINTER_SOLSTICE_DATE          355
#define WINTER_SOLSTICE_SUNRISE_HOUR  7
#define WINTER_SOLSTICE_SUNRISE_MIN   21
#define WINTER_SOLSTICE_SUNSET_HOUR   16
#define WINTER_SOLSTICE_SUNSET_MIN    53

////////////////////////////////////////////////////////////////////////////////
// End of file
////////////////////////////////////////////////////////////////////////////////
