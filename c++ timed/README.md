# C++ Timed Blinds Controller

This project uses C++ to control three sets of window blinds to open thirty minutes after sunrise and close thirty minutes before sunset. Super simple and reliable.